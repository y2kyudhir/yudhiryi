package com;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // write your code here

        File contract = new File("contracts.csv");
        File awarded = new File("awards.csv");

        if (contract.exists()) {
            try {
                //Read Each line
                List<String> eachlinecontracts;
                eachlinecontracts = Files.readAllLines(contract.toPath());


                List<String> eachlineawards;
                eachlineawards = Files.readAllLines(awarded.toPath());


                //Split and make Arrays
                String[][] contractsarray = new String[eachlinecontracts.size()][8];


                for (int i = 0; i < eachlinecontracts.size(); i++)
                {
                    contractsarray[i] = eachlinecontracts.get(i).split(",");
                }


                String[][] awardsarray = new String[eachlineawards.size()][6];

                for (int i = 0; i < eachlineawards.size(); i++)
                {
                    awardsarray[i] = eachlineawards.get(i).split(",");
                }


                String[][] finalarrayallforcsv = new String[eachlinecontracts.size()][13];// size of all fields and all contacts
                //Mapping
//                0  1  2  3  4  5  6  7  8  9  10  11  12
//
//                c0 c1 c2 c3 c4 c5 c6 c7 a1 a2 a3   a4  a5
                //start copying Contacts
                for (int j = 0; j < eachlinecontracts.size(); j++)
                {
                    for (int i = 0; i < 8; i++) //Contacts.csv Columns
                    {
                        finalarrayallforcsv[j][i] = contractsarray[j][i];
                    }
                }
                List<Integer> matching = new ArrayList<Integer>();
                for (int i = 0; i < eachlineawards.size(); i++)
                {
                    for (int j = 0; j < eachlinecontracts.size(); j++)
                    {
                        if (awardsarray[i][0].equalsIgnoreCase(contractsarray[j][0])) {
                            matching.add(j);
                        }
                    }
                }

                //Mapping
//                 0  1  2  3  4  5  6  7  8  9  10  11  12
//
//                c0 c1 c2 c3 c4 c5 c6 c7 a1 a2 a3   a4  a5

                //Awards Array
                int p = 0;
                for (int j = 0; j < eachlinecontracts.size(); j++) //Awards.csv Columns
                {
                    if (matching.contains(j)) {
                        for (int l = 1; l < 6; l++)
                        {
                            finalarrayallforcsv[j][7 + l] = awardsarray[p][l];
                        }
                        p++;
                    } else {
                        for (int l = 1; l < 6; l++)
                        {
                            finalarrayallforcsv[j][7 + l] = "";
                        }
                    }
                }

                PrintStream output = new PrintStream(new File("final.csv"));

                for (String[] arr : finalarrayallforcsv)
                {
                    //remove the square bracket o/p from the file
                    output.println(Arrays.toString(arr).replaceAll("\\[", "").replaceAll("\\]", ""));
                }
                int ContractWorth = 0;
                for (int i = 0; i < eachlinecontracts.size(); i++)
                {
                    if (finalarrayallforcsv[i][1].equals("Closed") && !(finalarrayallforcsv[i][12] == "")) {
                        ContractWorth += Integer.parseInt(finalarrayallforcsv[i][12]);
                    }
                }
                System.out.println("Total Amount of closed contracts: " + ContractWorth);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
